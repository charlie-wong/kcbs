filter-true = $(strip $(filter-out 1 ON On on TRUE True true,$1))
filter-false = $(strip $(filter-out 0 OFF Off off FALSE False false,$1))

THIS_DIR = $(shell pwd)

# If IS_VERBOSE equals 0 then the commands will be hidden.
# If IS_VERBOSE equals 1 then the commands will be displayed.
IS_VERBOSE := 0
ifeq ("$(origin V)", "command line")
    IS_VERBOSE := $(V)
endif

ifneq (, $(VERBOSE))
    IS_VERBOSE := 1
endif

# Local Build Configuration
-include local.mk

ifneq (, $(strip $(shell (command -v ninja))))
    GENERATOR ?= 'Ninja'
    BUILD_CMD ?= $(shell (command -v ninja))
    ifeq (1, $(IS_VERBOSE))
        # Only need to handle VERBOSE for Ninja,
        # Make will inherit the VERBOSE variable
        BUILD_CMD += -v
    endif
else
    BUILD_CMD ?= $(MAKE)
    GENERATOR ?= 'Unix Makefiles'
endif

BUILD_DIR ?= $(THIS_DIR)/build
ifneq (1, $(words [$(BUILD_DIR)]))
    $(error BUILD_DIR must not contain whitespace)
endif

BUILD_TYPE ?= Release
CMAKE_PROG ?= $(shell (command -v cmake || echo cmake))
BUILD_ARGS ?= -DCMAKE_BUILD_TYPE=$(BUILD_TYPE) $(THIS_DIR)

ifneq (, $(INSTALL_PERFIX))
    BUILD_ARGS += -DCMAKE_INSTALL_PREFIX=$(INSTALL_PERFIX)
endif

# Do not show cmake warnings for none 'Debug' build
ifneq (Debug, $(BUILD_TYPE))
    BUILD_ARGS += -Wno-dev
endif

# Check Qt5 library install prefix
ifneq (,$(USE_SHARED_QT5))
    BUILD_ARGS += -DUSE_SHARED_QT5=ON
    BUILD_ARGS += -DCMAKE_PREFIX_PATH=$(USE_SHARED_QT5)
endif

ifneq (,$(USE_STATIC_QT5))
    BUILD_ARGS += -DUSE_STATIC_QT5=ON
    BUILD_ARGS += -DCMAKE_PREFIX_PATH=$(USE_STATIC_QT5)
endif

ifneq (,$(EXTRA_BUILD_ARGS))
    BUILD_ARGS += $(EXTRA_BUILD_ARGS)
endif

PHONY := all
all: | $(BUILD_DIR)/.ran-cmake
	$(BUILD_CMD) -C $(BUILD_DIR)

$(BUILD_DIR)/.ran-cmake:
	mkdir -p $(BUILD_DIR)
	cd $(BUILD_DIR) && $(CMAKE_PROG) -G $(GENERATOR) $(BUILD_ARGS)

PHONY += install
install: all
	$(BUILD_CMD) -C $(BUILD_DIR) install

PHONY += clean
clean:
	rm -rf $(BUILD_DIR)
	$(MAKE) -C kbuild clean

# kconfig testing
PHONY += test
test: all
	@python3 -B -m pytest $(THIS_DIR)/tests \
        -o cache_dir=$(BUILD_DIR)/cache \
        $(if $(findstring 1,$(IS_VERBOSE)),--capture=no)

# kconfig update, sync to latest kernel
PHONY += update
update:
	scripts/ksync.sh $(K)

.PHONY: $(PHONY)
