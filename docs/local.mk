# Buidl Tools
# BUILD_DIR := build.msys
# GENERATOR := 'MSYS Makefiles'
#
# BUILD_DIR := build.ninja
# GENERATOR := 'Ninja'

# Build Type
# BUILD_TYPE := Debug

# Install Prefix
# INSTALL_PERFIX:= $(THIS_DIR)/usr

# If both set or not, build static kconfig
# EXTRA_BUILD_ARGS += -DUSE_STATIC_KCONFIG=ON
# EXTRA_BUILD_ARGS += -DUSE_SHARED_KCONFIG=ON

# Set Kconfig root menu, default is ''
# EXTRA_BUILD_ARGS += -DKCONFIG_ROOTMENU=MyRootMenu

# Set Kconfig prefix, default is 'CONFIG_'
# EXTRA_BUILD_ARGS += -DKCONFIG_PREFIX=awesome_


# About Qt5
# Set the Qt5 install perfix like the following lines.
# This is the location directory which contains:
#   bin/, lib/, include/, share/, etc.
#   - Given the Qt5 static install prefix
#   - Given the Qt5 shared install prefix
# Use the system wide if both not setting, auto detected
# NOTE: they are mutually exclusive!
# USE_SHARED_QT5 := /lib/x86_64-linux-gnu
# USE_SHARED_QT5 := /usr/lib/x86_64-linux-gnu
# USE_SHARED_QT5 := /lib/i386-linux-gnu
# USE_SHARED_QT5 := /usr/lib/i386-linux-gnu
# USE_SHARED_QT5 := D:/App/Msys2/mingw64
# USE_SHARED_QT5 := D:/App/Msys2/mingw32
# USE_STATIC_QT5 := /opt/charlie/lib64/qt-5.9.1
# USE_STATIC_QT5 := /opt/charlie/lib64/qt-5.9.1
# USE_STATIC_QT5 := D:/App/Msys2/mingw64/qt5-static
# USE_STATIC_QT5 := D:/App/Msys2/mingw64/qt5-static
