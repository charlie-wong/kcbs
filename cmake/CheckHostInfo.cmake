# ARGC  actual args number pass to function when called
# ARGV  actual args list when called(all)
# ARGV0 number 1 actual argument
# ARGV1 number 2 actual argument
# ARGV2 number 3
# ...
# ARGN  when call function with more arguments then want,
#       this is the list of the that part args

function(get_host_name_user_name_linux user_name host_name)
    find_program(WHOAMI_PROG   whoami)
    find_program(HOSTNAME_PROG hostname)

    # get user name
    if(EXISTS ${WHOAMI_PROG})
        execute_process(COMMAND ${WHOAMI_PROG}
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        OUTPUT_VARIABLE    COMPILER_NAME)
        set(${user_name} "${COMPILER_NAME}" CACHE
            INTERNAL "Host User Name" FORCE)
    else()
        if(DEFINED ENV{USERNAME})
            set(${user_name} "$ENV{USERNAME}" CACHE
                INTERNAL "Host User Name" FORCE)
        elseif()
            set(${user_name} "anonymous" CACHE
                INTERNAL "Host User Name" FORCE)
        endif()
    endif()

    # get host name
    if(EXISTS ${HOSTNAME_PROG})
        execute_process(COMMAND ${HOSTNAME_PROG}
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        OUTPUT_VARIABLE    HOST_NAME)
        set(${host_name} "${HOST_NAME}" CACHE
            INTERNAL "Host Name" FORCE)
    else()
        if(DEFINED ENV{HOSTNAME})
            set(${host_name} "$ENV{HOSTNAME}" CACHE
                INTERNAL "Host Name" FORCE)
        elseif()
            set(${host_name} "anonymous" CACHE
                INTERNAL "Host Name" FORCE)
        endif()
    endif()
endfunction()

function(get_host_name_user_name_windows user_name host_name)
    set(${user_name} "$ENV{USERNAME}" CACHE
        INTERNAL "Host User Name" FORCE)
    set(${host_name} "$ENV{USERDOMAIN}" CACHE
        INTERNAL "Host Name" FORCE)
endfunction()

function(get_host_name_user_name_macosx user_name host_name)
    find_program(WHOAMI_PROG   whoami)
    find_program(HOSTNAME_PROG hostname)

    # get user name
    if(EXISTS ${WHOAMI_PROG})
        execute_process(COMMAND ${WHOAMI_PROG}
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        OUTPUT_VARIABLE     COMPILER_NAME)

        set(${user_name} "${COMPILER_NAME}" CACHE
            INTERNAL "Host User Name" FORCE)
    else()
        set(${user_name} "anonymous" CACHE
            INTERNAL "Host User Name" FORCE)
    endif()

    # get host name
    if(EXISTS ${HOSTNAME_PROG})
        execute_process(COMMAND ${HOSTNAME_PROG} -s
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        OUTPUT_VARIABLE     HOST_NAME)

        set(${host_name} "${HOST_NAME}" CACHE
            INTERNAL "Host Name" FORCE)
    else()
        set(${host_name} "anonymous" CACHE
            INTERNAL "Host Name" FORCE)
    endif()
endfunction()

function(get_host_system_info_linux os_name os_version)
    find_program(LSB_RELEASE_PROG  lsb_release)
    if(EXISTS ${LSB_RELEASE_PROG})
        execute_process(COMMAND ${LSB_RELEASE_PROG} -i
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        OUTPUT_VARIABLE  distributor_id)
        execute_process(COMMAND ${LSB_RELEASE_PROG} -r
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        OUTPUT_VARIABLE  release_version)

        string(REGEX REPLACE "^Release:[ \t]*([0-9.]*)$"
                             "\\1" release_version "${release_version}")
        string(REGEX REPLACE "^Distributor ID:[ \t]*([^ ]*)$"
                             "\\1" distributor_id "${distributor_id}")

        set(${os_name} "${distributor_id}" CACHE
            INTERNAL "Host System Name" FORCE)
        set(${os_version} "${release_version}" CACHE
            INTERNAL "Host System Version" FORCE)
        return()
    endif()

    execute_process(COMMAND cat /etc/issue
                    OUTPUT_STRIP_TRAILING_WHITESPACE
                    ERROR_QUIET
                    RESULT_VARIABLE  read_status
                    OUTPUT_VARIABLE  issue_txt)
    if(read_status EQUAL 0)
        string(REGEX REPLACE "^[ \t]*([^0-9. ]*)[ \t]*[0-9].*"
                             "\\1" distributor_id "${issue_txt}")
        string(REGEX REPLACE "^[ \t]*[a-zA-Z]*[^0-9.]*[ \t]*([0-9.]+)[ \t]*.*"
                             "\\1" release_version "${issue_txt}")

        set(${os_name} "${distributor_id}" CACHE
            INTERNAL "Host System Name" FORCE)
        set(${os_version} "${release_version}" CACHE
            INTERNAL "Host System Version" FORCE)
        return()
    endif()

    message(AUTHOR_WARNING "[linux] => add support for host linux distribution.")
    set(${os_name} "Linux" CACHE
        INTERNAL "Host System Name" FORCE)
    set(${os_version} "" CACHE
        INTERNAL "Host System Version" FORCE)
endfunction()

function(get_host_system_info_windows os_name os_version)
    set(${os_version} "${CMAKE_HOST_SYSTEM_VERSION}" CACHE
        INTERNAL "Host System Version" FORCE)

    string(SUBSTRING "${CMAKE_HOST_SYSTEM_VERSION}" 0 4 windows_major_version)
    if(windows_major_version STREQUAL "6.10")
        set(${os_name} "Windows 10" CACHE
            INTERNAL "Host System Name" FORCE)
        return()
    endif()

    string(SUBSTRING "${CMAKE_HOST_SYSTEM_VERSION}" 0 3 windows_major_version)
    if(windows_major_version STREQUAL "6.1")
        set(${os_name} "Windows 7" CACHE
            INTERNAL "Host System Name" FORCE)
        return()
    endif()

    if(windows_major_version STREQUAL "6.2"
       OR windows_major_version STREQUAL "6.3")
        set(${os_name} "Windows 8" CACHE
            INTERNAL "Host System Name" FORCE)
        return()
    endif()

    set(${os_name} "Windows" CACHE
        INTERNAL "Host System Name" FORCE)
endfunction()

function(get_host_system_info_macosx os_name os_version)
    find_program(SW_VERS_PROG  sw_vers)

    if(EXISTS ${SW_VERS_PROG})
        execute_process(COMMAND ${SW_VERS_PROG} -productName
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        OUTPUT_VARIABLE  _mac_pro_name)
        execute_process(COMMAND ${SW_VERS_PROG} -productVersion
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        OUTPUT_VARIABLE  _mac_pro_version)
        execute_process(COMMAND ${SW_VERS_PROG} -buildVersion
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        OUTPUT_VARIABLE  _mac_build_version)
        set(_macosx_version "${_mac_pro_version}-${_mac_build_version}")

        set(${os_name} "${_mac_pro_name}" CACHE
            INTERNAL "Host System Name" FORCE)
        set(${os_version} "${_macosx_version}" CACHE
            INTERNAL "Host System Version" FORCE)
    else()
        message(WARNING "[Macos] => current Macos system has no 'sw_vers' found.")
        set(${os_name} "Macos" CACHE
            INTERNAL "Host System Name" FORCE)
        set(${os_version} "" CACHE
            INTERNAL "Host System Version" FORCE)
    endif()
endfunction()

function(get_current_system_time_linux _sys_time)
    find_program(DATE_PROG date)

    if(EXISTS ${DATE_PROG})
        execute_process(COMMAND ${DATE_PROG} "+%Y-%m-%d\ %T\ %z"
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        OUTPUT_VARIABLE  _cur_date_time)

        set(${_sys_time} "${_cur_date_time}" CACHE
            INTERNAL "Current Timestamp" FORCE)
    else()
        set(${_sys_time} "xxxx-xx-xx xx:xx:xx" CACHE
            INTERNAL "Current Timestamp" FORCE)
    endif()
endfunction()

function(get_current_system_time_windows _sys_time)
    # see: https://stackoverflow.com/questions/5300572/show-execute-process-output-for-commands-like-dir-or-echo-on-stdout
    # cmd /c echo %date:~0,4%-%date:~5,2%-%date:~8,2% %time:~0,2%:%time:~3,2%:%time:~6,2%
    execute_process(COMMAND cmd /c echo %date:~0,4%-%date:~5,2%-%date:~8,2%
                    OUTPUT_STRIP_TRAILING_WHITESPACE
                    OUTPUT_VARIABLE _cur_date)
    execute_process(COMMAND cmd /c time /T
                    OUTPUT_STRIP_TRAILING_WHITESPACE
                    OUTPUT_VARIABLE _cur_hhmm)
    execute_process(COMMAND cmd /c echo %time:~6,2%
                    OUTPUT_STRIP_TRAILING_WHITESPACE
                    OUTPUT_VARIABLE _cur_ss)
    set(_cur_date_time "${_cur_date} ${_cur_hhmm}:${_cur_ss}")
    set(${_sys_time} "${_cur_date_time}" CACHE
        INTERNAL "Current Timestamp" FORCE)
endfunction()

function(get_current_system_time_macosx _sys_time)
    get_current_system_time_linux(${_sys_time})
endfunction()

function(GetHostNameUserName _user_name _host_name)
    if(HOST_OS_LINUX)
        get_host_name_user_name_linux(${_user_name} ${_host_name})
    elseif(HOST_OS_WINDOWS)
        get_host_name_user_name_windows(${_user_name} ${_host_name})
    elseif(HOST_OS_MACOS)
        get_host_name_user_name_macosx(${_user_name} ${_host_name})
    endif()
endfunction()

function(GetHostSystemInfo _host_name _host_version)
    if(HOST_OS_LINUX)
        get_host_system_info_linux(${_host_name} ${_host_version})
    elseif(HOST_OS_WINDOWS)
        get_host_system_info_windows(${_host_name} ${_host_version})
    elseif(HOST_OS_MACOS)
        get_host_system_info_macosx(${_host_name} ${_host_version})
    endif()
endfunction()

function(GetCurrentSystemTime cur_time)
    if(HOST_OS_LINUX)
        get_current_system_time_linux(${cur_time})
    elseif(HOST_OS_WINDOWS)
        get_current_system_time_windows(${cur_time})
    elseif(HOST_OS_MACOS)
        get_current_system_time_macosx(${cur_time})
    endif()
endfunction()
